// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or vendor/assets/javascripts of plugins, if any, can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/sstephenson/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery_ujs
//= require fancybox
//= require js-image-slider
// require turbolinks
// require_tree .

$(document).ready(function() {
	/* Search */
	$('.button-search').bind('click', function() {
		$('#searches_item').submit()
		
	});
	$("a.image-selector").fancybox({
	});
	
	 $("a.fancybox").fancybox();
	$('.submit_search').click(function(){
	  $('#searches_item').submit();
	})
	
	setInterval(function(){
	  $('#banner_right, #banner_left').hide("slide")
	  setTimeout(function(){
	    $.ajax({
	      url: '/banner',
	      method: 'get',
	      success: function(){}
	    })
	  }, 5000)
	}, 60000)
	$('#leave_message_popup').hide();
	$('#leave_message').click(function(){
	  $(this).hide();
	  $('#leave_message_popup').show();
	})
	
	$('.minimalize_popup').click(function(){
	  $('#leave_message_popup').hide();
	  $('#leave_message').show();
	})
	
	$(".for_phone").ForceNumericOnly();
	if($(window).width() >= 1300){
	  $('.content').css('margin-left','20px')
	}
	if($(window).width() == 1024){
	  $('#column-right').hide();
	}
	if($(window).width() < 1024){
	  $('#column-left').hide();
	  $('#column-right').hide();
	}
	
})

$(function(){
//Menu
$('.categories li').mouseenter(function(){	
		$(this).find('ul').first()
			   .stop(true,true).slideDown('fast');
	}).mouseleave(function(){
		$(this).find('ul').first()
			   .stop(true,true).slideUp('fast');
	});
});

//$(function(){
//	//Homepage Slideshow
//	$('.slides').cycle({ 
//		speed: 200, 
//		timeout: 3000, 
//		fx : 'scrollLeft',
//		pager: '.slideTab', 
//		pagerEvent: 'mouseover', 
//		pause: 1 
//	});	
//	$('.slideTab a').wrap("<li></li>");
//	$('.slides li').mouseenter(function(){
//		$(this).find('p').stop(true,true).animate({bottom: '0px'},200);
//	}).mouseleave(function(){
//		$(this).find('p').stop(true,true).animate({bottom: '-36px'},200);
//	});
//});



$(function(){
	//Best Sellers
	$(".bestSeller li").mouseenter(function(){			
		$(this).find(".details").show();		
	}).mouseleave(function(){
		$(this).find(".details").hide();
	});
});


$(function(){
	//Slideshow
	var li = $(".slides li");
		li.hide().first().show();
		$(".slideTabs li").first().addClass("active");
		$(".slideTabs li").mouseenter(function(){
			$(".slideTabs li").removeClass("active");
			var attr = $(this).attr("class");			
			$(this).addClass("active");
				$(".slides li").hide();
				$(".slides li."+attr).fadeIn(1000);
		});
});

// FUNCTION IS NUMERICABLE
jQuery.fn.ForceNumericOnly =
function()
{
   return this.each(function()
   {
     $(this).keydown(function(e)
     {
       var key = e.charCode || e.keyCode || 0;
         console.log(e.key)
         keys = ['1','2','3','4','5','6','7','8','9','0','+']
         return (
           !(jQuery.inArray(e.key, keys) < 0)
        );
     });
   });
};


