class HomeController < ApplicationController
  before_filter :all_case
  
  def index
    @items = Item.all
    @items = Kaminari.paginate_array(@items).page(params[:page]) if @items.class == Array
    @items = @items.page(params[:page])
  end
  
  def page
    slug = unslug(params[:page])
    @page = Page.find_by_name(slug)
    return ( not_found ) unless @page
  end
  
  def item
    slug = unslug(params[:item])
    cslug = unslug(params[:category])
    c = Category.find_by_name(cslug)
    return ( not_found ) unless c
    @item = Item.find_by_name_and_category_id(slug, c.id)
    return ( not_found ) unless @item
  end
  
  def category
    slug = unslug(params[:category])
    @category = Category.find_by_name(slug)
    return ( not_found ) unless @category
    @items = @category.items
    @items = Kaminari.paginate_array(@items).page(params[:page]) if @items.class == Array
    @items = @items.page(params[:page])
  end
  
  def process_buy
    item_id = params[:item_id]
    handphone = params[:telepon]
    address = params[:alamat]
    name = params[:name]
    f = Feedback.new(item_id: item_id, address: address, name: name, handphone: handphone)
    if f.save
      flash[:notice] = 'terima kasih, transaksi anda akan segera kami proses'
    else
      flash[:error] = 'transaksi tidak bisa kami proses, silahkan ulangi kembali'
    end
    redirect_to :back
  end
  
  def search
    @s = params[:filter_name]
    if @s.present?
      @items = Item.where('name LIKE ?',"%#{@s}%")
      return ( not_found ) unless @items
      @items = Kaminari.paginate_array(@items).page(params[:page]) if @items.class == Array
      @items = @items.page(params[:page])
    else
      c = Category.find_by_id(params[:categories]).name.split(' ').join('-') rescue '-'
      redirect_to category_path(c)
    end
  end
  
  def contact_us
    f = Contact.new(params[:contact_us])
    if f.save
      flash[:notice] = 'kirim pesan berhasil'
    else
      flash[:error] = 'kirim pesan ke admin gagal, harap isi semua field dengan benar'
    end
    redirect_to :back
  end
  
  def banner
    @banners_left = Banner.left.sample(3)
    @banners_right = Banner.right.sample(3)
    respond_to :js
  end
  
  private
  def not_found
    flash[:error] = 'page not found' and redirect_to root_path
  end
  
  def all_case
    @categories = Category.all
    @pages = Page.all
    @hot_items = Item.hots
    @favorite_items = Item.favorites
    @banners_left = Banner.left.sample(6)
    @banners_right = Banner.right.sample(6)
  end
  
  def unslug(slug)
    slug.split('-').join(' ') rescue ''
  end
  
end
