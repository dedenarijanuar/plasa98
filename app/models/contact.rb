class Contact < ActiveRecord::Base
  attr_accessible :name, :handphone, :email, :message
  validates :name, :handphone, :email, :message, presence: {message: 'tidak boleh kosong'}
end
