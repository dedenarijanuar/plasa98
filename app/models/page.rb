class Page < ActiveRecord::Base
  attr_accessible  :content, :name
  
  def self.create_additional_page
    pages = ['galeri video','cek ongkos kirim','cek resi JNE']
    
    pages.each do |p|
      Page.create(name: p, content: "ini adalah halaman untuk #{p}")
    end

  end
end
