class Banner < ActiveRecord::Base
  PLACE = ['kanan','kiri']
  attr_accessible :image, :link_url, :place
  mount_uploader :image, ImageUploader
  
  scope :left, -> { where(place: 'kiri') }
  scope :right, -> { where(place: 'kanan') }
  
  def place_from
    self.place == 'kiri' ? 'kiri' : 'kanan'
  end
end
