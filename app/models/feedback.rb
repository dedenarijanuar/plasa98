class Feedback < ActiveRecord::Base
  belongs_to :item
  attr_accessible :item_id, :handphone, :address, :name
  validates :item_id, :handphone, :address, :name, presence: {message: 'tidak boleh kosong'}
end
