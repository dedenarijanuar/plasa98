class Image < ActiveRecord::Base
  
  belongs_to :item
  attr_accessible :image, :item_id
  mount_uploader :image, ImageUploader
end
