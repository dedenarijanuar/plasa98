class Item < ActiveRecord::Base
  has_many :feedbacks
  belongs_to :category
  has_many :images
  attr_accessible :name, :amount, :rate, :description, :is_available, :category_id, :is_hot, :images_attributes, :is_favorite
  accepts_nested_attributes_for :images
  paginates_per 40
  
  validates :name, :amount, :rate, :description, :category, presence: {message: 'tidak boleh kosong'}
  validate :rating
  
  default_scope order('updated_at DESC')
  
  scope :hots, -> { where(is_hot: true).limit(5) }
  scope :favorites, -> { where(is_favorite: true).limit(5) }
  
  def image_seconds
    self.images.delete_if{|x| self.images.first.id == x.id } rescue []
  end
  
  def rating
    errors.add(:rate, "rating harus 1 sampai 5") if self.rate.to_i > 5
  end
  
  def mini_url
    self.images.first.image.url(:mini) rescue "/assets/image1.jpg"
  end
  
  def thumb_url
    self.images.first.image.url(:thumb) rescue ""
  end
  
  def large_url
    self.images.first.image.url(:large) rescue ""
  end
  
  def available?
    self.is_available?? 'Tersedia' : 'Tidak tersedia'
  end
end
