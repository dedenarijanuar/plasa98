module ApplicationHelper
  def slug(obj)
    obj.name.split(' ').join('-') rescue ''
  end
end
