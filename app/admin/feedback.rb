ActiveAdmin.register Feedback, as: 'Pemesanan' do
  index do
    selectable_column
    column "Tanggal" do |i|
      i.created_at.to_date
    end
    column "Nama" do |i|
      i.name
    end
    column "Handphone" do |i|
      i.handphone
    end
    column "Alamat" do |i|
      i.address
    end
    column "Barang yang di pesan" do |i|
      i.item.name rescue '-'
    end
    actions
  end
  
  filter :name, :label => 'Nama'
  filter :handphone, :label => 'Handphone'
  
  show do |i|
    attributes_table do
      row "Tanggal" do |i|
        i.created_at.to_date
      end
      row "Nama" do |i|
        i.name
      end
      row "Handphone" do |i|
        i.handphone
      end
      row "Alamat" do |i|
        i.address
      end
      row "Barang yang di pesan" do |i|
        i.item.name
      end
    end
  end
  
  actions :all, :except => [:new, :edit]


end
