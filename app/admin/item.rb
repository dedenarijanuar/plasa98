ActiveAdmin.register Item, as: 'Barang' do
  
  index do
    selectable_column
    column "Tanggal" do |i|
      i.created_at.to_date
    end
    column "Nama" do |i|
      i.name
    end
    column "Kategori" do |i|
      i.category.name.titleize
    end
    column "Harga" do |i|
      i.amount.to_i.display
    end
    column "Rating" do |i|
      i.rate
    end
    column "Produk Terlaris" do |i|
      i.is_hot?? "YES" : 'NO'
    end
    column "Produk Unggulan" do |i|
      i.is_favorite?? "YES" : 'NO'
    end
    column "Stok Barang" do |i|
      i.available?
    end
    actions
  end
  
  form do |i|
    i.inputs do
      i.input :name, label: 'Nama'
      i.input :category
      i.input :rate, label: 'Rating'
      i.input :amount, label: 'Harga'
      i.input :description, label: 'Deskripsi'
      i.input :is_hot, label: 'Produk Terlaris'
      i.input :is_available, label: 'Stok Barang'
      i.input :is_favorite, label: 'Produk Unggulan'
      i.has_many :images do |m|
        m.input :image, :hint => m.template.image_tag(m.object.image.url(:large))
        m.actions do
          m.action :submit
        end
      end
      i.action :submit
    end
  end
  
  filter :name, :label => 'Nama'
  filter :category, :label => 'Kategori'
  filter :amount, :label => 'Harga'
  filter :rate, :label => 'Rating'
  filter :is_hot, :label => 'Produk Terlaris'
  filter :is_availabe, :label => 'Stok Barang'
  filter :is_favorite, :label => 'Produk Unggulan'
  
  show do |i|
    attributes_table do
      row "Nama" do
        i.name.titleize
      end
      row "Kategori" do
        i.category.name.titleize
      end
      row "Harga" do
        i.amount.to_i.display
      end
      row "Rating" do
        i.rate
      end
      row "Produk Terlaris" do
        i.is_hot?? "YES" : 'NO'
      end
      row "Produk Unggulan" do
        i.is_favorite?? "YES" : 'NO'
      end
      row "Stok Barang" do
        i.available?
      end
      row "Deskripsi" do
        i.description
      end
      row "Item Images" do
        ul do
          i.images.each do |m|
            li do 
              image_tag(m.image.url(:mini))
            end
          end
        end
      end
    end
  end

end
