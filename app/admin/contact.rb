ActiveAdmin.register Contact, as: 'KontakPesan' do
index do
    selectable_column
    column "Tanggal" do |i|
      i.created_at.to_date
    end
    column "Nama" do |i|
      i.name
    end
    column "Handphone" do |i|
      i.handphone
    end
    column "Email" do |i|
      i.email
    end
    actions
  end
  
  filter :name, :label => 'Nama'
  filter :handphone, :label => 'Handphone'
  filter :email, :label => 'Email'
  
  show do |i|
    attributes_table do
      row "Nama" do
        i.name.titleize
      end
      row "Handphone" do
        i.handphone
      end
      row "Email" do
        i.email
      end
      row "Pesan" do
        i.message
      end
    end
  end
  
  actions :all, :except => [:new, :edit]

end
