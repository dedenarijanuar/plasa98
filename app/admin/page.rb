ActiveAdmin.register Page, as: "HalamanKonten" do

  index do
    selectable_column
    id_column
    
    column "Title" do |article|
      article.name
    end
    column "Content" do |article|
      ActionView::Base.full_sanitizer.sanitize(article.content.truncate(200))
    end
  end
  
  filter :name

  form(:html => { :multipart => true }) do |f|
    f.inputs "Blog Details" do
      f.input :name
      f.input :content, :as => :ckeditor
    end
    f.actions
  end
  
  show do |s|
    render "admin/pages/show", page: s
  end
  actions :all, :except => [:new, :destroy]
end
