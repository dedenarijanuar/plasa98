ActiveAdmin.register Banner, as: 'IklanBanner' do
  index do
    selectable_column
    column "Tanggal" do |i|
      i.created_at.to_date
    end
    column "Gambar" do |i|
      image_tag(i.image.url(:mini))
    end
    column "Tata Letak" do |i|
      i.place_from
    end
    column "Direct Link URL" do |i|
      i.link_url
    end
    actions
  end
  
  form do |i|
    i.inputs do
      i.input :image, label: 'Gambar Iklan'
      i.input :link_url, label: 'Direct Link URL'
      i.input :place, as: 'select', collection: Banner::PLACE, label: 'Tata Letak'
      i.action :submit
    end
  end
  
  filter :link_url, :label => 'Direct Link URL'
  
  show do |i|
    attributes_table do
      row "Tanggal" do |i|
        i.created_at.to_date
      end
      row "Gambar" do |i|
        image_tag(i.image.url(:large))
      end
      row "Tata Letak" do |i|
        i.place_from
      end
      row "Direct Link URL" do |i|
        i.link_url
      end
    end
  end


end
