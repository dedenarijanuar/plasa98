ActiveAdmin.register Category, as: 'Kategori' do
index do
    selectable_column
    column "Tanggal" do |i|
      i.created_at.to_date
    end
    column "Nama" do |i|
      i.name
    end
    actions
  end
  
  form do |i|
    i.inputs do
      i.input :name, label: 'Nama'
      i.action :submit
    end
  end
  
  filter :name, :label => 'Nama'
  
  show do |i|
    attributes_table do
      row "Nama" do
        i.name.titleize
      end
    end
  end

end
