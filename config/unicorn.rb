working_directory "/home/plasa98/plasa98"
pid "/home/plasa98/plasa98/pids/unicorn.pid"
stderr_path "/home/plasa98/plasa98/log/unicorn.log"
stdout_path "/home/plasa98/plasa98/log/unicorn.log"

# Unicorn socket
listen "/tmp/unicorn.plasa98.sock"

# Number of processes
# worker_processes 4
worker_processes 2

# Time-out
timeout 30
