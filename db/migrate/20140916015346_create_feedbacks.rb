class CreateFeedbacks < ActiveRecord::Migration
  def change
    create_table :feedbacks do |t|
      t.string :item_id
      t.string :handphone
      t.string :address
      t.string :name

      t.timestamps
    end
  end
end
