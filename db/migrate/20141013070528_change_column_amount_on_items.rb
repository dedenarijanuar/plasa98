class ChangeColumnAmountOnItems < ActiveRecord::Migration
  def change
    change_column :items, :amount, :string
  end
end
