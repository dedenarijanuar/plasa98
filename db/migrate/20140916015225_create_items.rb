class CreateItems < ActiveRecord::Migration
  def change
    create_table :items do |t|
      t.string :name
      t.integer :amount
      t.integer :rate
      t.text :description
      t.boolean :is_available
      t.integer :category_id
      t.boolean :is_hot

      t.timestamps
    end
  end
end
