pages = ['profil','mekanisme belanja','syarat dan ketentuan','faq']
categories = ['fashion','komputer','handphone','kamera','game','alat rumah tangga']

['pages','categories','items','feedbacks','images','banners'].each do |x|
  ActiveRecord::Base.connection.execute("TRUNCATE #{x}")
end

pages.each do |p|
  Page.create(name: p, content: "ini adalah halaman untuk #{p}")
end

Page.create_additional_page

categories.each do |c|
  Category.create(name: c)
end

20.times do |i|
  Item.create(name: "samsung galaxy #{i+1}", amount: "#{rand(8)+1}000000", rate: 5, is_available: true, description: "ii adalah deskripsi tentang samsung galaxy #{i}", category_id: Category.all.sample.id, is_hot: true )
end

1.upto(3) do |i|
  Banner.create(image: File.open("#{Rails.root.to_s}/app/assets/images/iklan#{i}.jpg"), link_url: 'http://beliapaaja.com')
end

10.times do
  Image.create(image: File.open("#{Rails.root.to_s}/app/assets/images/image1.jpg"), item_id: Item.all.sample.id)
end
